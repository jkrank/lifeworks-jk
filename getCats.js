const http = require('./http')
const { getAge, sortBy, formatAge, baseEndPoint } = require('./utils')

module.exports = async () => {
    let cats = []
    const groups = new Map([
        ['ginger', []],
        ['black', []],
        ['others', []],
    ])
    try {
        const response = await http(`${baseEndPoint}/cats`)
        cats = []
        response.body.forEach((cat) => {
            const catObj = {
                fullName: `${cat.forename} ${cat.surname}`,
                type: 'cat',
                age: getAge(cat.dateOfBirth),
                image: cat.image.url,
            }
            if (groups.has(cat.colour)) {
                groups.get(cat.colour).push(catObj)
            } else {
                groups.get('others').push(catObj)
            }
        })
        groups.forEach((group) => {
            cats.push(...sortBy(group, 'age', 'desc'))
        })
        cats.map((cat) => {
            cat.age = formatAge(cat.age)
            return cat
        })
    } catch (e) {
        cats = 'error'
    }
    return cats
}