const moment = require('moment')

const getAge = (dateOfBirth) => {
    const today = moment()
    const birthDay = moment(dateOfBirth)

    return today.diff(birthDay, 'months')
}

const formatAge = (age) => {
    return `${Math.floor(age / 12)} years, ${age % 12} months`
}

const sortBy = (array, key, direction) => {
    return array.sort((a, b) => {
        const first = a[key]
        const second = b[key]
        return (direction === 'desc' ? -1 : 1)
            * (first < second ? -1 : +(first > second))
    })
}

module.exports = {
    getAge,
    formatAge,
    sortBy,
    baseEndPoint: 'https://apigateway.test.lifeworks.com/rescue-shelter-api',
}