const nodeHttp = require('http')
const getDogs = require('./getDogs')
const getCats = require('./getCats')
const getHamsters = require('./getHamsters')

const handler = async () => {
    let animals = ''
    let code = 200
    let response = {}
    try {
        animals = await Promise.all([
           getDogs(),
           getCats(),
           getHamsters(),
        ])
        if (animals.every(a => a === 'error')) {
            code = 500
            response = {
                error: 'Internal Server Error'
            }
        } else {
            response = animals.reduce((a, b) => {
                if (Array.isArray(b)) {
                    a.push(...b)
                }
                return a
            }, [])
        }
    } catch (e) {
        code = 500
        response = {
            error: ' Internal Server Error'
        }
    }
    return {
        code,
        response,
    }
}

nodeHttp.createServer(async (req, res) => {
    const { code, response } = await handler(req)
    res.writeHead(code, {'Content-Type': 'application/javascript'});
    res.write(JSON.stringify(response))
    res.end()
}).listen(8080)

module.exports = {
    handler,
}