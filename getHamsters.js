const http = require('./http')
const { getAge, sortBy, formatAge, baseEndPoint } = require('./utils')

module.exports = async () => {
    let hamsters = []
    try {
        const response = await http(`${baseEndPoint}/hamsters`)
        hamsters = response.body.map(hamster => ({
            fullName: `${hamster.forename} ${hamster.surname}`,
            type: 'hamster',
            age: getAge(hamster.dateOfBirth),
            image: hamster.image.url,
        }))
        hamsters = sortBy(hamsters, 'age', 'asc')
            .map((hamster) => {
                hamster.age = formatAge(hamster.age)
                return hamster
            })
    } catch (e) {
        hamsters = 'error'
    }
    return hamsters
}