const nock = require('nock')
const assert = require('assert')
const moment = require('moment')
const { handler } = require('../index')
const getDogs = require('../getDogs')
const getHamsters = require('../getHamsters')
const getCats = require('../getCats')

describe('get 500', () => {
    before(() => {
        const scope = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/*')
            .reply(500, {})
    })
    it('should return 500 when all fail', (done) => {
        handler().then((result) => {
            assert.equal(JSON.stringify(result), JSON.stringify({ code: 500, response: { error: 'Internal Server Error' }}))
            done()
        }).catch((e) => { done(new Error(e)); })
    })
})


describe('get partial', () => {
    before(() => {
        const dogs = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/dogs')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'a','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                ]
            })
        const hamsters = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/hamsters')
            .reply(500, {})
        const cats = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/cats')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'a','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                ]
            })
    })

    after(() => {
        nock.cleanAll()
    })
    it('should return partial when some fail', (done) => {
        handler().then((result) => {
            assert.equal(JSON.stringify(result), JSON.stringify(
                {
                    "code":200,
                    "response":[
                        {"fullName":"1 a","type":"dog","age":"0 years, 2 months","image":"1.jpg"},
                        {"fullName":"1 a","type":"cat","age":"0 years, 2 months","image":"1.jpg"}
                    ]
                }
            ))
            done()
        }).catch((e) => { done(new Error(e)); })
    })
})

describe('get all', () => {
    before(() => {
        const dogs = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/dogs')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'dog','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                ]
            })
        const hamsters = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/hamsters')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'hamster','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                ]
            })
        const cats = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/cats')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'cat','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                ]
            })
    })

    after(() => {
        nock.cleanAll()
    })
    it('should return partial when some fail', (done) => {
        handler().then((result) => {
            assert.equal(JSON.stringify(result), JSON.stringify(
                {
                    "code":200,
                    "response":[
                        {"fullName":"1 dog","type":"dog","age":"0 years, 2 months","image":"1.jpg"},
                        {"fullName":"1 cat","type":"cat","age":"0 years, 2 months","image":"1.jpg"},
                        {"fullName":"1 hamster","type":"hamster","age":"0 years, 2 months","image":"1.jpg"}
                    ]
                }
            ))
            done()
        }).catch((e) => { done(new Error(e)); })
    })
})