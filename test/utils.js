const assert = require('assert')
const moment = require('moment')
const { getAge, formatAge, sortBy } = require('../utils')

describe('Utils', function() {
  describe('getAge', function() {
    it('should return age in months', () => {
      assert.equal(getAge(moment().subtract(13, 'months')), 13)
      assert.equal(getAge(moment().subtract(2, 'months')), 2)
      assert.equal(getAge(moment().subtract(10, 'years')), 120)
    })
    it(`should format age in 'years, months'`, () => {
        assert.equal(formatAge(13), '1 years, 1 months')
        assert.equal(formatAge(2), '0 years, 2 months')
        assert.equal(formatAge(25), '2 years, 1 months')
    })
    it('should sort array by given key and direction', () => {
        let array = [{age: 2}, {age: 20}, {age: 10}]
        assert.equal(JSON.stringify(sortBy(array, 'age', 'asc')), JSON.stringify([{age: 2}, {age: 10}, {age: 20}]))
        assert.equal(JSON.stringify(sortBy(array, 'age', 'desc')), JSON.stringify([{age: 20}, {age: 10}, {age: 2}]))
    })
  })
})