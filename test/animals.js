const nock = require('nock')
const assert = require('assert')
const moment = require('moment')
const getDogs = require('../getDogs')
const getHamsters = require('../getHamsters')
const getCats = require('../getCats')

describe('get animal (200)', function () {
    before(function() {
        const dogs = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/dogs')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'a','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                    {'forename':'2','surname':'b','dateOfBirth': moment().subtract(23, 'months'),'image':{'url':'2.jpg'}},
                    {'forename':'3','surname':'c','dateOfBirth': moment().subtract(30, 'months'),'image':{'url':'3.jpg'}},
                ]
            })
        
        const cats = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/cats')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'ginger','dateOfBirth': moment().subtract(2, 'months'), colour: 'ginger', image: {'url':'1.jpg'}},
                    {'forename':'2','surname':'blue','dateOfBirth': moment().subtract(3, 'months'),colour: 'blue', 'image': {'url':'2.jpg'}},
                    {'forename':'1','surname':'black','dateOfBirth': moment().subtract(2, 'months'),colour: 'black', 'image': {'url':'2.jpg'}},
                    {'forename':'1','surname':'other','dateOfBirth': moment().subtract(23, 'months'),colour: 'white', 'image': {'url':'2.jpg'}},
                    {'forename':'2','surname':'ginger','dateOfBirth': moment().subtract(30, 'months'), colour: 'ginger', image: {'url':'3.jpg'}},
                    {'forename':'2','surname':'black','dateOfBirth': moment().subtract(3, 'months'),colour: 'black', 'image': {'url':'2.jpg'}},
                ]
            })
            
        const hamsters = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/hamsters')
            .reply(200, {
                'body':[
                    {'forename':'1','surname':'a','dateOfBirth': moment().subtract(2, 'months'),'image':{'url':'1.jpg'}},
                    {'forename':'2','surname':'b','dateOfBirth': moment().subtract(23, 'months'),'image':{'url':'2.jpg'}},
                    {'forename':'3','surname':'c','dateOfBirth': moment().subtract(30, 'months'),'image':{'url':'3.jpg'}},
                ]
            })
    })

    after(() => {
        console.log('afterAll')
        nock.cleanAll()
    })

    it('/dogs should return 200 sorted array', (done) => {   
        getDogs().then(function (result) {
            assert.equal(JSON.stringify(result), JSON.stringify([
                {'fullName':'3 c', type: 'dog', 'age': '2 years, 6 months', 'image': '3.jpg'},
                {'fullName':'2 b', type: 'dog', 'age': '1 years, 11 months', 'image': '2.jpg'},
                {'fullName':'1 a', type: 'dog', 'age': '0 years, 2 months', 'image': '1.jpg'},
            ]))
            done()
        }).catch(function (e) { done(new Error(e)); })
    })

    it('/cats should return 200 sorted array', (done) => { 
        getCats().then(function (result) {
             assert.equal(JSON.stringify(result), JSON.stringify([
                {'fullName':'2 ginger', type: 'cat', 'age': '2 years, 6 months', 'image': '3.jpg'},
                {'fullName':'1 ginger', type: 'cat', 'age': '0 years, 2 months', 'image': '1.jpg'},
                {'fullName':'2 black', type: 'cat', 'age': '0 years, 3 months', 'image': '2.jpg'},
                {'fullName':'1 black', type: 'cat', 'age': '0 years, 2 months', 'image': '2.jpg'},
                {'fullName':'1 other', type: 'cat', 'age': '1 years, 11 months', 'image': '2.jpg'},
                {'fullName':'2 blue', type: 'cat', 'age': '0 years, 3 months', 'image': '2.jpg'},
            ]))
            done()
        }).catch(function (e) { done(new Error(e)); })
    })

    it('/hamsters should return 200 sorted array', (done) => {   
        getHamsters().then(function (result) {
            assert.equal(JSON.stringify(result), JSON.stringify([
                {'fullName':'1 a', type: 'hamster', 'age': '0 years, 2 months', 'image': '1.jpg'},
                {'fullName':'2 b', type: 'hamster', 'age': '1 years, 11 months', 'image': '2.jpg'},
                {'fullName':'3 c', type: 'hamster', 'age': '2 years, 6 months', 'image': '3.jpg'},
            ]))
            done()
        }).catch(function (e) { done(new Error(e)); })
    })
})

describe('get animal (500)', function () {
    before(function() {
        const dogs = nock('https://apigateway.test.lifeworks.com/rescue-shelter-api')
            .get('/*')
            .reply(500, {})
    })

    after(() => {
        console.log('afterAll')
        nock.cleanAll()
    })

    it('/dogs should return 500 error', (done) => {   
        getDogs().then(function (result) {
            assert.equal(result, 'error')
            done()
        }).catch(function (e) { done(new Error(e)); })
    })

    it('/cats should return 500 error', (done) => {   
        getDogs().then(function (result) {
            assert.equal(result, 'error')
            done()
        }).catch(function (e) { done(new Error(e)); })
    })

    it('/hamsters should return 500 error', (done) => {   
        getDogs().then(function (result) {
            assert.equal(result, 'error')
            done()
        }).catch(function (e) { done(new Error(e)); })
    })

})
