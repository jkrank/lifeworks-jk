### To run
- npm install
-  npm start

The endpoint is available at localhost:8080
The index.js exports handler, which is the main event handler, so it should transform
easily to lambda endpoint

### To run the tests
- npm test

Individual pet types, 200 or 500 response, and utility functions are covered by test