const http = require('./http')
const { getAge, sortBy, formatAge, baseEndPoint } = require('./utils')

module.exports = async () => {
    let dogs = []
    try {
        const response = await http(`${baseEndPoint}/dogs`)
        dogs = response.body.map(dog => ({
            fullName: `${dog.forename} ${dog.surname}`,
            type: 'dog',
            age: getAge(dog.dateOfBirth),
            image: dog.image.url,
        }))
        dogs = sortBy(dogs, 'age', 'desc')
            .map((dog) => {
                dog.age = formatAge(dog.age)
                return dog
            })
    } catch (e) {
        dogs = 'error'
    }
    return dogs
}